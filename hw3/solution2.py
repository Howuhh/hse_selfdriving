from gym_duckietown.tasks.task_solution import TaskSolution
import numpy as np
import cv2


class DontCrushDuckieTaskSolution(TaskSolution):
    def __init__(self, generated_task):
        super().__init__(generated_task)
        self.yellow_amount = []
        self.side = 3 # right
        self.env = self.generated_task['env']

    @staticmethod
    def get_yellow_amount(img):
        lower = np.array([0, 10, 85])
        upper = np.array([60, 255, 255])
        mask = cv2.inRange(cv2.cvtColor(img, cv2.COLOR_RGB2HSV), lower, upper)
        return mask.sum()

    def solve(self):
        img, _, _, _ = self.env.step([0, 0])

        condition = True
        while condition:
            img, _, _, _ = self.env.step([1, 0])
            
            self.yellow_amount.append(self.get_yellow_amount(img))

            # side = 3 (вместо 1) здесь исключительно ради 360 noscope флекса
            # драйвер нереальный - профессиональный

            # turn to the left
            if self.side == 3 and np.mean(self.yellow_amount[-50:]) > 15e5:
                self.turn(100, self.side * 1)
                self.move(50, 1)
                self.turn(100, self.side * -1)
                self.side = -3

            # turn to the right
            elif self.side == -3 and self.yellow_amount[-1] > 22e5:
                self.move(30, 10)
                
                self.turn(100, self.side * 1)
                self.move(50, 1)
                self.turn(100, self.side * -1)
                
                condition = False

            self.env.render()
        
        self.move(30, 10)
        self.env.step([0, 0])

    def move(self, steps, amount):
        for _ in range(steps):
            self.env.render()
            img, _, _, _ = self.env.step(np.array([amount * 0.1, 0]))
            self.yellow_amount.append(self.get_yellow_amount(img))

    def turn(self, steps, amount):
        for _ in range(steps):
            self.env.render()
            img, _, _, _ = self.env.step(np.array([0, amount * 1]))
            self.yellow_amount.append(self.get_yellow_amount(img))