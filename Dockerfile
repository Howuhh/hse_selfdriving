FROM osrf/ros:noetic-desktop-full
ENV SHELL=/bin/bash

ENV TZ=Europe/Moscow
RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone

RUN apt-get update \
    && apt-get install -y wget vim tmux \
    && wget https://repo.anaconda.com/miniconda/Miniconda3-latest-Linux-x86_64.sh \
    && bash Miniconda3-latest-Linux-x86_64.sh -b \
    && rm -f Miniconda3-latest-Linux-x86_64.sh \
    && echo PATH="/root/miniconda3/bin":$PATH >> .bashrc \
    && echo "source /opt/ros/noetic/setup.bash" >> .bashrc \
    && echo "source ~/.bashrc" >> .bashrc \
    && exec bash

ENV PATH /root/miniconda3/bin:$PATH
RUN conda update --all
RUn conda install python=3.8 \
    && conda install -c conda-forge numpy scipy pydot pyqt pyside2 ros-rospy
