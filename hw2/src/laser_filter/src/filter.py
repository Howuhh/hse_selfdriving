#!/root/miniconda3/bin/python
import rospy

import numpy as np
from sensor_msgs.msg import LaserScan
from nav_msgs.msg import OccupancyGrid, MapMetaData


class Filter:
    def __init__(self, window=2, threshold=1, res=0.1, radius=10):
        rospy.Subscriber('/base_scan', LaserScan, self.filter_callback)
        self.map_publisher = rospy.Publisher('/map_topic', OccupancyGrid, queue_size=10)
        self.rate = rospy.Rate(20)

        self.window = window
        self.thr = threshold
        self.res = res
        self.radius = radius
        self.grid_size = 2 * int(radius / res) + 10

    def filter_callback(self, msg):
        points = np.array(msg.ranges) 
        points = points[self._filter(points)]

        grid_data = self._make_grid(points, msg.angle_min, msg.angle_max)

        # make Grid message
        grid = OccupancyGrid()
        grid.info = MapMetaData()

        grid.header.frame_id = "base_laser_link"
        grid.info.resolution = self.res

        grid.info.width = self.grid_size
        grid.info.height = self.grid_size

        grid.info.origin.position.x = -self.radius
        grid.info.origin.position.y = -self.radius
        grid.info.origin.position.z = 0

        grid.data = grid_data.ravel()
        self.map_publisher.publish(grid)

    def _filter(self, points):
        mask = np.zeros(len(points), dtype=bool)

        for i, point_dist in enumerate(points[self.window:-self.window]):
            left = points[i - self.window:i]
            right = points[i + 1:i + 1 + self.window]

            mean_dist = (left.sum() + right.sum()) / (2 * self.window)

            if np.abs(point_dist - mean_dist) / mean_dist < self.thr:
                mask[i] = True

        return mask

    def _make_grid(self, points, angle_min, angle_max):
        angles = np.linspace(angle_min, angle_max, len(points), endpoint=True)
        grid = np.zeros((self.grid_size, self.grid_size), dtype=int)

        xs = points * np.cos(angles)
        ys = points * np.sin(angles)

        for x, y in zip(xs, ys):
            if x**2 + y**2 < self.radius **2:
                i = int((x + self.radius) / self.res)
                j = int((y + self.radius) / self.res)
                grid[j, i] = 100

        return grid


if __name__ == "__main__":
    rospy.init_node("laser_filter")
    filter_ = Filter()
    rospy.spin()    
