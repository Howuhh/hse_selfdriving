docker run -it \
    --rm \
    --volume="/home/alex/All/programming/Python/uni/hse_selfdriving:/home/hse_selfdriving" \
    --env="DISPLAY" \
    --env="QT_X11_NO_MITSHM=1" \
    --volume="/tmp/.X11-unix:/tmp/.X11-unix:rw" ros