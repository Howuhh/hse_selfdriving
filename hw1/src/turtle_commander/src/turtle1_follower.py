#!/root/miniconda3/bin/python
import rospy
import numpy as np

from turtlesim.msg import Pose
from geometry_msgs.msg import Twist


def create_twist_msg(dist, angle):
    msg = Twist()
    msg.linear.x = dist
    msg.angular.z = angle
    return msg


class FollowerNode:
    def __init__(self, turtle1_topic, turtle2_topic):
        self.turtle1 = np.array([0, 0])

        rospy.Subscriber(f"/{turtle1_topic}/pose", Pose, self.update_turtle1)
        rospy.Subscriber(f"/{turtle2_topic}/pose", Pose, self.move_turtle2)

        self.turtle2_pub = rospy.Publisher(f"/{turtle2_topic}/cmd_vel", Twist, queue_size=10)

    def update_turtle1(self, msg):
        self.turtle1[0] = msg.x
        self.turtle1[1] = msg.y

    def move_turtle2(self, msg):
        turtle2 = np.array([msg.x, msg.y])
        n = np.array([np.cos(msg.theta), np.sin(msg.theta)])

        diff = self.turtle1 - turtle2
        norm = np.linalg.norm(diff)
        diff = diff / norm

        angle = np.arccos(diff @ n) * np.sign(np.cross(n, diff))

        self.turtle2_pub.publish(create_twist_msg(np.log1p(norm), angle))

rospy.init_node("turtle_follower")
FollowerNode("turtle1", "turtle2")
rospy.spin()







