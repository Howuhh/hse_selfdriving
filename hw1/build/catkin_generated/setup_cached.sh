#!/usr/bin/env sh
# generated from catkin/python/catkin/environment_cache.py

# based on a snapshot of the environment before and after calling the setup script
# it emulates the modifications of the setup script without recurring computations

# new environment variables

# modified environment variables
export CMAKE_PREFIX_PATH="/home/hse_selfdriving/hw1/devel:$CMAKE_PREFIX_PATH"
export PWD='/home/hse_selfdriving/hw1/build'
export ROSLISP_PACKAGE_DIRECTORIES='/home/hse_selfdriving/hw1/devel/share/common-lisp'
export ROS_PACKAGE_PATH="/home/hse_selfdriving/hw1/src:$ROS_PACKAGE_PATH"